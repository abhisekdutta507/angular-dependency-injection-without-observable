import { Post } from "./post";
import { Album } from "./album";

export interface StoreData {
    albums: Album[],
    posts: Post[]
}
