import { Component } from '@angular/core';
import { StoreService } from './store.service';
import { StoreData } from './store-data';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public title = 'DependencyInjection';

  constructor(private store: StoreService) {}

  async wait(n: number) {
    return new Promise(next => {
      setTimeout(() => next(true), n * 1000);
    });
  }

  async ngOnInit() {
    await this.wait(2);

    const promiseArray = [
      fetch('https://jsonplaceholder.typicode.com/posts'),
      fetch('https://jsonplaceholder.typicode.com/albums')
    ];
    const responses = await Promise.all(promiseArray);
    const responsesJson = await Promise.all(responses.map(async response => (await response.json())));

    const data: StoreData = {
      posts: responsesJson[0],
      albums: responsesJson[1]
    };
    this.store.data = data;
  }
}
