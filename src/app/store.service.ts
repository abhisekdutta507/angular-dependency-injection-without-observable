import { Injectable } from '@angular/core';
import { StoreData } from './store-data';

@Injectable({
  providedIn: 'root'
})
export class StoreService {
  public data: StoreData = {
    albums: [],
    posts: []
  };

  constructor() { }
}
