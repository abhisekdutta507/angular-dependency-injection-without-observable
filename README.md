This project is created with [Angular CLI](https://angular.io/cli).
We have typescript support in this project using [typescript](https://code.visualstudio.com/docs/typescript/typescript-compiling) with [webpack-cli](https://webpack.js.org/guides/installation/).

Press  `Cmd + Shift + V`  form your Mac keyboard to preview the README.md.

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode. Then, visit http://localhost:4200 through a browser window to run the application.

## Learn More

You can learn more in the [Angular @Component({...})](https://angular.io/api/core/Component).

### Simple Angular Directives Setup In An Angular Project

**Initialize the project with `NPM`.** *Make sure you have a stable version of* [NodeJS](https://nodejs.org/en/), [NPM](https://www.npmjs.com/) & [Angular CLI](https://angular.io/cli) *is installed in your system globally.*

```bash
ng new angular-dependency-injection
```

Answer some questions asked by the `Angular CLI` when initiating the new project.

Please visit [@Injectable](https://angular.io/guide/singleton-services#singleton-services) & [DI](https://angular.io/guide/dependency-injection) for more info.

**Description** - **DI** is useful when we want to share a piece of code throughout our Angular application. We can create a single instance of the service class in the memory. We also call this approach as a Singleton pattern.

Before `Angular 6.0` we had to pass in the service class as a parameter in the `providers` array of its parent `@NgModule`. Now we can pass the `providedIn` attribute in the `@Injectable` decorator.

```typescript
import { Injectable } from '@angular/core';
import { StoreData } from './store-data';

@Injectable({
  providedIn: 'root'
})
export class StoreService {
  public data: StoreData = {
    albums: [],
    posts: []
  };

  constructor() { }
}
```

Checkout the `app.component.ts` too.

```typescript
import { Component } from '@angular/core';
import { StoreService } from './store.service';
import { StoreData } from './store-data';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public title = 'DependencyInjection';

  constructor(private store: StoreService) {}

  async ngOnInit() {
    const promiseArray = [
      fetch('https://jsonplaceholder.typicode.com/posts'),
      fetch('https://jsonplaceholder.typicode.com/albums')
    ];
    const responses = await Promise.all(promiseArray);
    const responsesJson = await Promise.all(responses.map(async response => (await response.json())));

    const data: StoreData = {
      posts: responsesJson[0],
      albums: responsesJson[1]
    };
    this.store.data = data;
  }
}
```

Using the service data in different components. Please check the the `albums.component.ts`.

```ts
import { Component, OnInit } from '@angular/core';
import { StoreService } from '../store.service';

@Component({
  selector: 'app-albums',
  templateUrl: './albums.component.html',
  styleUrls: ['./albums.component.scss']
})
export class AlbumsComponent implements OnInit {

  constructor(public store: StoreService) { }

  ngOnInit(): void {
  }

}
```

Also, check the `albums.component.html`.

```html
<div class="ph-16">
  <ng-container *ngFor="let album of store?.data?.albums">
    <div class="card mv-16">
      <p class="p-0 m-0">ID: {{album?.id}}</p>
      <p class="p-0 m-0">User ID: {{album?.userId}}</p>
      <p class="p-0 m-0">Title: {{album?.title}}</p>
    </div>
  </ng-container>
</div>
```

Hurray!! we have learned the basic concepts of Angular dependency injection.